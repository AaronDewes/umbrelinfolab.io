Click [here](/de/) for a German version of this page.
A Spanish version is also available [here](/es/)

---

## Additional resources about Umbrel
{: .no_toc }

This page features additional information about Umbrel that can't be found on the official website.
This page is intended to answer your questions about Umbrel.

---

 This page is not an official Umbrel page.
 In addition, this page only applies to the latest version of Umbrel (currently v0.3.2).

---

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## About this guide

### Structure

1. Introduction (this page)
2. [Troubleshooting](troubleshooting.html): Issues concerning the primary operations of your Umbrel node.
3. [General FAQ](faq.html): General questions and further learnings regarding the technology used in Umbrel.

---

## A word of caution

Umbrel is still in beta and should not be considered secure.
We do not recommend running it with much money yet to avoid loosing money.

---

This page is part of [Umbrel Labs](https://UmbrelLabs.gitlab.io), a free service extending your Umbrel node.

---

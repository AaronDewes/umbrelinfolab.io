## Zusätzliche Ressourcen zu Umbrel
{: .no_toc }

Diese Webseite enthält Informationen zu Umbrel, die es aud der ofiziellen Website noch nicht gibt.
Diese Seite soll eure Fragen zu Umbrel beantworten.

---

 Dies ist keine ofizielle Umbrel-Seite.
 Außerdem bezieht sich die Seite immer nur auf die neueste Version von Umbrel (aktuell v0.3.2).

---

## Inhaltsverzeichnis
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Über diese Seite

### Struktur

1. Einleitung (Diese Seite)
2. [Troubleshooting](troubleshooting.html): Fragen zu Umbrel und der Verwendung von Umbrel.
3. [General FAQ](faq.html): Generelle Fragen und weitere Informationen zu der in Umbrel eingesetzten Technologie

---

## Eine kleine Warnung

Umbrel ist noch in der Entwicklung und sollte nicht als komplett sicher gelten.
Wir raten davon ab, es mit viel Geld zu verwenden, um Verluste zu vermeiden.

---

Diese Seite ist Teil von [Umbrel Labs](https://UmbrelLabs.gitlab.io), ein kostenloser Service, der dein Node erweitert.

---

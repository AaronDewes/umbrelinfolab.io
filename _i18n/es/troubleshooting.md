# Solución de Problemas
{: .no_toc }

Problemas respecto a las funciones principales de tu nodo Umbrel.

Preguntas generales e información acerca de la tecnología usada en Umbrel se encuentra en la guía [Preguntas Frecuentes](faq.md).

## Tabla de contenido
{: .no_toc .text-delta }

1. TOC
{:toc}

### ¿Puedo acceder a mi nodo Umbrel usando SSH?

¡Claro que sí! Abre una terminal en tu ordenador e introduce `ssh umbrel@umbrel.local` y presionas ENTER. Después introducirás la contraseña `moneyprintergobrrr`. Puedes cambiar esta contraseña por defecto accediendo a tu nodo via SSH y ejecutar el comando `passwd`.

### ¿Es mi modelo de Raspberry Pi compatible con Umbrel?

Si cuentas con una Raspberry Pi con por lo menos 4GB de RAM, puedes correr Umbrel en ella. 

### Mi nodo Umbrel no arranca. ¿Qué puedo hacer?

¿Tienes conectado algo a los pins GPIO?
Si es así, desconéctalo y reinicia la RPi desconectando la fuente de poder y volviéndola a conectar.

### No puedo acceder al dashboard/menú principal en umbrel.local o Mi Nodo sigue fallando. ¿Qué puedo hacer?
Revisa que tu router detecte a tu nodo.

Si no lo detecta, o tu cable ethernet está conectado de manera incorrecta, o el nodo no arranca.
Si no crees que el cable ethernet sea el problema, sigue los pasos de la pregunta previa.

Si tu router detecta a tu nodo, trata de acceder a él directamente con la dirección IP. 
Si tampoco puedes acceder al dashboard/menú principal con la dirección IP, prueba a ejecutar una herramienta automática que encuentra errores [via SSH](#can-i-login-using-ssh):

```
~/umbrel/scripts/debug
```

El resultado de la ejecución te dirá los pasos a seguir automáticamente.

### Quiero conectarme a mi nodo usando ...... con mi red local, pero no funciona. ¿Cómo puedo arreglar esto?

Si quieres conectarte a tu nodo Umbrel con tu red local solamente reemplaza el dominio onion por umbrel.local para cualquier conección de cadena de caracteres.

### Configurando una dirección IP fija para la Raspberry Pi

Si tu router no soporta configurar una dirección IP estática para un solo dispositivo, también puedes hacerlo directamente en la Raspberry Pi. 

Esto puede ser hecho configurando al cliente DHCP (en la RPi) para que este le anuncie una dirección IP estática al Servidor DHCP (normalmente el router) antes de que automáticamente asigne una diferente a la Raspberry Pi.

1. Obtener la dirreción IP de la puerta de salida (router)
   Ejecutar `netstat -r -n` y elegir la columna de salida que no es `0.0.0.0`. En mi caso es `192.168.178.1`.

2. Configurar la dirección estática para la Rpi, la puerta de salida y un Servidor DNS.
   La configuración para el cliente DHCP (en RPi) está localizada en el archivo `/etc/dhcpcd.conf`:

   ```
   sudo nano /etc/dhcpcd.conf
   ```

   El siguiente fragmento es un ejemplo de configuración. Cambia el valor de `static routers` y `static domain_name_servers` por la dirección IP de tu router (puerta de salida por defecto) del paso 1. Se consciente de darle una dirección IP a la Raspberry Pi que está **FUERA** del rango de direcciones asignadas por el servidor DHCP. Puedes mirar este rango en la página de configuración del router y checar en donde está el rango de direcciones DHCP. Esto significa que si el rango de direcciones DHCP va desde `192.168.178.1` a `192.168.178.99` estarías bien con la IP `192.168.178.100` para tu Raspberry Pi.

   Incluyélas en el archivo `/etc/dhcpcd.conf`:

   ```
   # Configuración de una dirección IP estática (CAMBIA LOS VALORES PARA QUE SEAN ADECUADAS PARA TU RED)
   interface eth0
   static ip_address=192.168.178.100/24
   static routers=192.168.178.1
   static domain_name_servers=192.168.178.1
   ```

3. Reiniciar el sistema de Red
   `sudo /etc/init.d/networking restart`

### Usar Wi-Fi en vez de Ethernet

- Crea un archivo `wpa_supplicant.conf` en la partición de arranque de la tarjeta MicroSD con el siguiente contenido.
  Importante: El nombre de la red (SSID) y la contraseña deben de estar en dobles comillas (como `psk="contraseña"`)

  ```conf
  ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
  update_config=1
  country=[CODIGO_PAIS]
  network={
    ssid="[SSID_WIFI]"
    psk="[CONTRASEÑA_WIFI]"
  }
  ```

- Reemplaza `[CODIGO_PAIS]` por el [codigo ISO2](https://www.iso.org/obp/ui/#search){:target="\_blank"} de tu país (ejem. `MX`)
- Replace `[SSID_WIFI]` y `[CONTRASEÑA_WIFI]` con los datos de tu red Wi-Fi.

### Accediendo manualmente a `bitcon-cli` y `lncli`

En Umbrel, estos binarios siempre estarán disponibles en UMBREL_ROOT_DIR/bin/. En Umbrel OS (Sistema Operativo Umbrel) puedes [acceder a ellos por SSH](#can-i-login-using-ssh) como:

```
~/umbrel/bin/bitcoin-cli
```

y

```
~/umbrel/bin/lncli
```

### Resetear tu nodo Umbrel y los datos de usuario (si perdiste tu constraseña)

Sólo has esto en el caso de que **NO** tengas fondos en tu cartera LND! Si tienes fondos, entocnes guarda tu semilla + archivo de respaldo para que puedas restaurarlo después si es necesario.

ADVERTENCIA: Perderás tu semilla, configuraciones, información y aplicaciones.

```
sudo systemctl stop umbrel-startup && sudo rm -rf ~/umbrel/lnd/!(lnd.conf) && sudo rm ~/umbrel/db/user.json && sudo rm ~/umbrel/db/umbrel-seed/seed && sudo systemctl start umbrel-startup
```

### Actualizar manualmente Umbrel

Para actualizar manualmente tu nodo, ejecuta estos comandos [accede via SSH](#can-i-login-using-ssh):

```
cd ~/umbrel && sudo ./scripts/update/update --repo getumbrel/umbrel#v0.3.2
```

Reemplaza v0.3.2 con la vesión a la que quisieras actualizar tu nodo.

Si la actualización estaba atorada, ejecuta este comando antes del comando de arriba:

```
sudo rm statuses/update-in-progress 
```

---

Esta guía de Solución de problemas será actualizada con fallos que han o serán reportados en la sección de errores. Eres libre de contribuír con recomendaciones.

---

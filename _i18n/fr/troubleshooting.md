# Résolution de problèmes
{: .no_toc }

Problèmes concernant les opérations principales de votre nœud Umbrel.

Des questions générales et des informations complémentaires concernant la technologie utilisée dans Umbrel peuvent être trouvées à la page [Questions fréquentes](faq.md). 

## Table des matières
{: .no_toc .text-delta }

1. Table des matières
{:toc}

### Puis-je me connecter via SSH?

Oui! Ouvrez un terminal sur votre oridnateur et entrez `ssh umbrel@umbrel.local` avec le mot de passe `moneyprintergobrrr`.

### Est-ce que mon Raspberry Pi est compatible?

Si vous avez un Raspberry Pi avec au moins 4GB de RAM, vous pouvez faire tourner Umbrel dessus.

### Mon noeud Umbrel n'arrête pas de planter. Qu'est-ce que je peux faire ?

Si vous n'utilisez pas l'alimentation électrique officielle, c'est probablement l'alimentation électrique.
Pour détecter une sous-tension, [connectez-vous à votre RPi via SSH] (# can-i-login-using-ssh) et exécutez cette commande: `vcgencmd get_throttled`.
S'il ne répond pas throttled = 0x0, c'est soit un problème avec le bloc d'alimentation, soit votre SSD qui utilise trop d'énergie (cela ne peut être le cas que si vous n'utilisez pas le matériel recommandé).

Si cela n'aide pas, [contactez-nous sur Telegram] (https://t.me/getumbrel). 

### Mon noeud Umbrel ne démarre pas. Qu'est-ce que je fais?

Avez-vous connecté quelque chose aux broches GPIO?
Si oui, essayez de les débrancher, et redémarrez le RPi en débranchant le bloc d'alimentation, puis en le rebranchant. 

### Je ne peux pas accéder au tableau de bord à umbrel.local ou mon noeud n'arrête pas de planter. Qu'est-ce que je peux faire?

Vérifiez si votre routeur détecte votre nœud.
Si ce n'est pas le cas, soit votre câble Ethernet n'est pas branché correctement, soit le nœud ne démarre pas.
Si vous pensez que le câble Ethernet n'est pas le problème, suivez la réponse à la question précédente.

S'il détecte le nœud, essayez d'y accéder directement avec l'adresse IP.
Si vous ne pouvez pas non plus accéder au tableau de bord via l'adresse IP, essayez d'exécuter le script de recherche automatique de problèmes [via SSH] (# can-i-login-using-ssh): 

```
~/umbrel/scripts/debug
```

Il vous indiquera automatiquement la prochaine étape. 

### Je veux me connecter à mon noeud en utilisant ...... sur mon réseau local, mais cela ne fonctionne pas. Comment puis-je faire? 

Si vous souhaitez vous connecter à votre Umbrel via le réseau local, remplacez simplement votre domaine onion par umbrel.local pour chaque chaîne de connexion. 

### Configurer une adresse fixe sur le Raspberry Pi

Si votre routeur ne prend pas en charge la configuration d'une adresse IP statique pour un seul appareil, vous pouvez également le faire directement sur le Raspberry Pi.

Cela peut être fait en configurant le client DHCP (sur le Pi) pour déclarer une adresse IP statique au serveur DHCP (souvent le routeur) avant qu'il n'en attribue automatiquement une autre au Raspberry Pi. 

1. Obtenez l'adresse IP de la passerelle par défaut (routeur).
    Exécutez `netstat -r -n` et choisissez l'adresse IP dans la colonne gateway qui n'est pas` 0.0.0.0`. Dans mon cas, c'est "192.168.178.1".

2. Configurez l'adresse IP statique du Pi, le chemin de la passerelle et un serveur DNS.
    La configuration du client DHCP (Pi) se trouve dans le fichier `/ etc / dhcpcd.conf`: 

   ```
   sudo nano /etc/dhcpcd.conf
   ```
   L'extrait de code suivant est un exemple de morceau de configuration. Remplacez la valeur de `static routers` et `static domain_name_servers` par l'IP de votre routeur (passerelle par défaut) de l'étpe 1. Attention de donner au Raspberry Pi une addresse **EN DEHORS** de la plage d'adresses qui sont attribuées par le serveur DHCP. Vous pouvez obtenir cette plage en regardant sous la page de configuration du routeur et en vérifiant la plage d'adresses DHCP. Cela signifie que si la plage DHCP va de `192.168.178.1` à` 192.168.178.99`, vous pouvez utiliser l'IP `192.168.178.100` pour votre Raspberry Pi. 

   Ajoutez ce qui suit au fichier `/etc/dhcpcd.conf` :

   ```
   # Configuration static IP address (CHANGE THE VALUES TO FIT FOR YOUR NETWORK)
   interface eth0
   static ip_address=192.168.178.100/24
   static routers=192.168.178.1
   static domain_name_servers=192.168.178.1
   ```

3. Redémarrez le réseau:
   `sudo /etc/init.d/networking restart`

### Utiliser le WIFI au lieu d'Ethernet

- Créez le fichier `wpa_supplicant.conf` dns la partition bootde la carte microSD avec le contenu suivant:
  Notez que le nom du réseau(ssid) et le mot de passe doivent être entre guillemets (par exemple `psk="password"`)

  ```conf
  ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
  update_config=1
  country=[COUNTRY_CODE]
  network={
    ssid="[WIFI_SSID]"
    psk="[WIFI_PASSWORD]"
  }
  ```

- Remplacez `[COUNTRY_CODE]` avec le [code ISO2](https://www.iso.org/obp/ui/#search){:target="\_blank"} de votre pays (par exemple `US`)
- Remplacez `[WIFI_SSID]` et `[WIFI_PASSWORD]` avec les identifiants de votre propre WIFI.

### Accéder manuellement à `bitcon-cli` et `lncli`

Sur Umbrel, ces programmes sont toujours disponibles dans UMBREL_ROOT_DIR/bin/. Avec Umbrel OS, vous pouvez y [accéder via SSH](#can-i-login-using-ssh) ainsi:

```
~/umbrel/bin/bitcoin-cli
```

et

```
~/umbrel/bin/lncli
```

### Réinitialiser vos données utilisateur Umbrel (si vous avez perdu votre mot de passe)

Ne faites ça que si vous **n'avez pas de fonds** dans votre portefeuille LND! Si vous avez des fonds, sauvegardez votre seed + et les fichiers de façon à pouvoir restaurer plus tard si nécessaire.

Vous allez perdre votre seed, vos paramètres, vos données et applications!

```
sudo systemctl stop umbrel-startup && sudo rm -rf ~/umbrel/lnd/!(lnd.conf) && sudo rm ~/umbrel/db/user.json && sudo rm ~/umbrel/db/umbrel-seed/seed && sudo systemctl start umbrel-startup
```

### Mettre à jour Umbrel manuellement

Pour mettre à jour votre noeud manuellement, executez ces commandes [via SSH](#can-i-login-using-ssh):

```
cd ~/umbrel && sudo ./scripts/update/update --repo getumbrel/umbrel#v0.3.2
```

Remplacez v0.3.2 avec la version vers laquelle vous voulez mettre à jour. 

Si la mise à jour a bloqué, executez ceci avant la commande précédente:


```
sudo rm statuses/update-in-progress 
```

---

Ce guide sur les questions les plus fréquentes sera continuellement mis à jour à mesure que les problèmes se présenteront. Les contributions sont les bienvenues via un pull request.

---

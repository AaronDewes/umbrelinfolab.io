Cliquer [ici](/de/) pour une version de cette page en allemand.
Une version en espagnol est également disponible [ici](/es/)

---

## Ressources additionnelles à propos de Umbrel
{: .no_toc }

Cette page contient des informations supplémentaires sur Umbrel qui ne peuvent pas être trouvées sur le site officiel.
Cette page est destinée à répondre à vos questions sur Umbrel. 

---

 Cette page n'est pas une page officielle d'Umbrel.
 De plus, cette page s'applique uniquement à la dernière version d'Umbrel (actuellement v0.3.2). 

---

## Table des matières
{: .no_toc .text-delta }

1. Table des matières
{:toc}

---

## A propos de ce guide

### Structure

1. Introduction (cette page)
2. [Résolution de problèmes](troubleshooting.html): Problèmes concernant les opérations principales de votre noeud Umbrel. 
3. [Questions fréquentes](faq.html): Questions générales et autres informations concernant la technologie utilisée dans Umbrel. 

---

## Petite mise en garde

Umbral est toujours en version bêta et ne doit pas être considéré comme sûr.
Nous ne recommandons pas de l'exécuter avec beaucoup d'argent pour le moment pour éviter de perdre de l'argent. 

---

Cette page fait partie de [Umbrel Labs](https://UmbrelLabs.gitlab.io), un service gratuit enrichissant votre noeud Umbrel.

---
